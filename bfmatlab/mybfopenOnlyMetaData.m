function [metadatastruct] = mybfopenOnlyMetaData( fileName)
% Extracts Meta Data from files
% 27.05.2013 (Version 1.0)
% Authors:
%   Andre Zeug (zeug.andre@mh-hannover.de)
%   Malte Butzlaff (Butzlaff.Malte@mh-hannover.de)
%
% This software is for non-commercial use only.
%% parsing imput parameters
p = inputParser;
p.StructExpand    = true;  % if we allow parameter  structure expanding
p.KeepUnmatched   = true;  % controls whether MATLAB throws an error (false) or not (true) when the function being called is passed an argument that has not been defined in the inputParser schema for this file.

p.addParameter('verbose'         , true  , @(x)(islogical(x) || any(x==[0 1 2])));
p.addParameter('gui'             , true  , @(x)(islogical(x) || any(x==[0 1 2 3])));
p.addParameter('timestamp'       , false , @(x)(isnumeric(x)));
par = p.Results;

% add unmached structure elements
s = fieldnames(p.Unmatched);
for n=1:length(s)
    par.(s{n}) = p.Unmatched.(s{n}); % '.(s{n})' acts like setfield(par, s(1), getfield(p.Unmatched, s{1})
end

% if there is no argument, we ask the user to choose a file:
if (nargin == 0)
    [fileName, pathname] = uigetfile('*.tif;*.stk;*.lsm;*.czi', 'select image file');
    par.fname = [pathname, fileName ];
    if isequal(fileName,0);return;end
else
    par.fname = fileName; %[pwd '\' fileName]; % require to replace since receive error when fileName contains full path
end

jPath = javaclasspath('-all');
isLociTools = cellfun(@(x) ~isempty(regexp(x, '.*bioformats_package.jar$', 'once')),jPath);
if ~any(isLociTools)
    locipath = which('bioformats_package.jar');
    if isempty(locipath)
        locipath = fullfile(fileparts(mfilename('fullpath')), 'bioformats_package.jar');
    end
    assert(exist(locipath, 'file') == 2, 'Cannot automatically locate bioformats_package.jar');

    % Add loci_tools to dynamic Java class path
    javaaddpath(locipath);
    %     if par.verbose; t1 = output('mybfopen: added ''bioformats_package.jar'' to java path', whos, toc(par.timestamp), t1); end
end

% Create a loci.formats.ReaderWrapper object
r = javaObject('loci.formats.ChannelSeparator', ...
    javaObject('loci.formats.ChannelFiller'));
% Initialize the metadata store
OMEXMLService = javaObject('loci.formats.services.OMEXMLServiceImpl');
r.setMetadataStore(OMEXMLService.createOMEXMLMetadata());
r.setId(par.fname);

% extract metadata table for this series
metadataOME = r.getMetadataStore();
iDX = metadataOME.getPixelsSizeX(0).getValue();
iDY = metadataOME.getPixelsSizeY(0).getValue();
iDC = metadataOME.getPixelsSizeC(0).getValue();
iDZ = metadataOME.getPixelsSizeZ(0).getValue();
iDT = metadataOME.getPixelsSizeT(0).getValue();
imageDims = [iDY iDX iDZ iDC iDT];
numImages = r.getImageCount();

%% transfer output
tt = par.fname;
tte=regexpi(tt,'/|\');
ttp=regexpi(tt,'\.');
if ~isempty(tte)
    if any(ttp>tte(end))
        tt = tt(tte(end)+1:ttp(end)-1);
    else
        tt = tt(tte(end)+1:end);
    end
end

%% metadataList
metadataList = r.getGlobalMetadata();
metadataKeys = metadataList.keySet().iterator();
if metadataList.size()>0
    % LA files contain no metadatalist
    % but Andor iQ and Zeiss ZEN do
    iic = 0;
    for ii=1:metadataList.size()
        iic = iic + 1;
        try
            key = metadataKeys.nextElement();
            info.list{iic,1} = strrep(key,[tt ' '],'');
            info.list{iic,2} = metadataList.get(key);
        catch
            info.list(iic,:)=[];
            iic = iic - 1;
        end
    end
    [~,ind] = sort(info.list(:,1));
    info.list = info.list(ind,:);
    % generate structure out of list info
    str = [];
    for ii =1:size(info.list,1)
        str = getStruckt(str, regexprep(regexprep(regexprep(info.list{ii,1},'\|',' '),'((?!\s)\W)',''),'\s*',' '), info.list{ii,2});
        % (?!test)expr => Match expr and do not match test.
        % \s: any whitespace character
        % \W: Any character that is not alphabetic, numeric, or underscore. (equivalent to [^a-zA-Z_0-9])
    end
    info.liststr = str;
else
    info.list{1,1} = 'no key';
    info.list{1,2} = 'no value';
    info.liststr = 'Empty';
end
%% metadataOME
% read timestamp from OME data
Ts = zeros(numImages,1);
try
    for nn=1:numImages
        Ts(nn) = metadataOME.getPlaneDeltaT(0,nn-1).value(); %remove .value() when using older versions of bioformats
    end
    info.Ts = reshape(Ts,[imageDims(3:end)]);
    info.TimeStamp = squeeze(info.Ts(1,1,:));%-info.Ts(1,1,1)); % time in seconds
    info.TStxt = 'TimeStamp for each frame';
catch
    if nn>1
        switch nn-1
            case iDT
                info.Ts = Ts(1:nn-1);
                info.TimeStamp = squeeze(info.Ts);
                info.TStxt = 'TimeStamp for each timepoint';
            case iDT*iDZ
                info.Ts = reshape(Ts(1:nn-1),[iDZ iDT]);
                info.TimeStamp = squeeze(info.Ts(1,:));%-info.Ts(1,1,1)); % time in seconds
                info.TStxt = 'TimeStamp for each z-section and timepoint';
            case iDT*iDZ*iDC
                info.Ts = reshape(Ts(1:nn-1),[iDC iDZ iDT]);
                info.TimeStamp = squeeze(info.Ts(1,1,:));%-info.Ts(1,1,1)); % time in seconds
                info.TStxt = 'TimeStamp for each channel, z-section and timepoint';
        end
    else
        info.Ts = 0;
        info.TimeStamp = 0;
        info.TStxt = 'No TimeStamp found.';
        warning('File contains no time information')
    end
end

info.OME = metadataOME;
metadatastruct=info;
% close file
r.close();
% inline function for structure generation from metadatalist
% recursive function call
    function str = getStruckt(str, tt, content)
        tt_pos = regexpi(tt,'[\s\|]');
        if isempty(tt_pos)
            if regexp(tt,'^\d*$')
                if str2double(tt)>0
                    str.no{str2double(tt)} = content;
                else
                    str.(['no' tt]) = content;
                end
            else
                ttn = regexprep(tt,'^\d*','');
                ttn = regexprep(ttn,'^_*','');
                str.(ttn) = content;
            end
        else
            if regexp(tt(1:tt_pos(1)-1),'^\d*$') % checks if tt is numerical
                ttn = ['no' tt(1:tt_pos(1)-1)];
            else
                ttn = tt(1:tt_pos(1)-1);
                ttn = regexprep(ttn,'^\d*','');
                ttn = regexprep(ttn,'^_*','');
            end
            %str_out = str_in.(tt);
            if ~isfield(str,ttn)
                str.(ttn)=[];
            elseif ~isstruct(str.(ttn))%% && ~iscell(str.(ttn))
                warning off
                str.(ttn).v = str.(ttn);
                warning on
            end
            str.(ttn) = getStruckt(str.(ttn), tt(tt_pos(1)+1:end), content);
        end
    end
end



