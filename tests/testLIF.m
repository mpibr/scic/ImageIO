% TEST LIF FILES
clc
clear variables
close all

%% add path
path_project = './';
addpath(genpath(path_project));

%% create pointer
file_lif = '../../230329.lif';

addpath(genpath('../'));
obj = imageIOPtr(file_lif);
disp([obj.wavelengthExc, obj.laserPower]);

