function obj = readRawMetadata(obj)
% READRAWMETADATA Access the raw metadata and sets the object properties
% The function accesses the binary metadata. The method takes as input the
% object filename and opens the file for binary read
% https://de.mathworks.com/matlabcentral/fileexchange/78089-rawlifreader
% https://github.com/bastibe/MatlabXML

    % open file for raw binary read
    fid = fopen(obj.fileFullPath, 'r', 'l');
    
    % read common block header
    CBHi = 0x70;  % common Block Header identifier (112)
    ID = fread(fid, 1, '*uint32');
    assert(ID == CBHi, 'Found %x instead of %x in first metadata block. Not a valid block.', ID, CBHi);
    CBHsize = fread(fid, 1, '*uint32');
    
    % read metadata block header
    MBHi = 0x2A;  % metadata Block Header identifier (42)
    ID = fread(fid, 1, '*uint8');
    assert(ID == MBHi, 'Found %x instead of %x in in first metadata block. Not a valid block.', ID, MBHi);
    MBHsize = fread(fid, 1, '*uint32');
    
    % read raw metadata block
    mdb = fread(fid, MBHsize, '*uint16');
    obj.originalMetadata = sprintf('%s', char(mdb));
    fclose(fid);
end